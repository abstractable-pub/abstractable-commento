#!/bin/bash

aws ssm put-parameter --name abstractable.io.prod001.commento.origin --value http://commento.abstractable.io --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.smtp.host --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.smtp.port --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.smtp.fromaddress --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.smtp.username --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.smtp.password --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.db.username --value foo --type SecureString --overwrite
aws ssm put-parameter --name abstractable.io.prod001.db.password --value foo --type SecureString --overwrite