#!/bin/bash

#
# thankyou mike g coleman (https://github.com/mikegcoleman/todo#docker-containers-with-docker-compose)
#

#
# function to source SSM parameter value using $1 as parameter key 
# and $2 as environment variable name to be exported
#
function get_parameter {
    PARAM_VAL=`aws ssm get-parameter --name $1 --with-decryption | jq -r .Parameter.Value`;
    if [[ -v $PARAM_VAL && ! -z ${PARAM_VAL} ]]; then
        echo "Can't set environment variable $2. Aborting ..."
        exit
    fi
    export $2=$PARAM_VAL
}

#
# perform updates/installs
#

# bring the OS up to date
sudo apt update && sudo apt upgrade -y

# install dependencies
sudo apt install jq

# install latest version of docker the lazy way
curl -sSL https://get.docker.com | sh

# make it so you don't need to sudo to run docker commands
sudo usermod -aG docker ubuntu

# install docker-compose
sudo curl -L https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

#
# get env vars
#

get_parameter abstractable.io.prod001.commento.origin COMMENTO_ORIGIN
get_parameter abstractable.io.prod001.smtp.host DB_USERNAME
get_parameter abstractable.io.prod001.smtp.port DB_PASSWORD
get_parameter abstractable.io.prod001.smtp.fromaddress SMTP_HOST
get_parameter abstractable.io.prod001.smtp.username SMTP_PORT
get_parameter abstractable.io.prod001.smtp.password SMTP_USERNAME
get_parameter abstractable.io.prod001.db.username SMTP_PASSWORD
get_parameter abstractable.io.prod001.db.password SMTP_FROM_ADDRESS

#
# create SSL certificate
#

sudo apt install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt update
sudo apt install certbot
sudo certbot certonly --standalone -d commento.abstractable.io

#
# install/launch the servicve
#

# copy the dockerfile into /srv/docker 
# if you change this, change the systemd service file to match
# WorkingDirectory=[whatever you have below]
sudo mkdir /srv/docker
sudo curl -o /srv/docker/docker-compose.yml https://gitlab.com/abstractable-pub/abstractable-commento/-/raw/master/docker-compose.yml 

# copy in systemd unit file and register it so our compose file runs 
# on system restart
sudo curl -o /etc/systemd/system/docker-compose-app.service https://gitlab.com/abstractable-pub/abstractable-commento/-/raw/master/docker-compose-app.service
sudo systemctl enable docker-compose-app

# create bind points
sudo mkdir -p /usr/share/nginx/html 

# start up the application via docker-compose
docker-compose -f /srv/docker/docker-compose.yml up -d