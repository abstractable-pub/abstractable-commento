# abstractable-commento

## Description

This project configures Commento using Docker in a Lightsail-managed EC2 instance.

## Procedure

1. From the Lightsail console click 'Create Instance'.
2. Choose a region.
3. Under 'Select a blueprint' click on 'OS Only' and choose an Ubuntu image.
4. Click on '+ Add launch script'.
5. Copy the `Launch Loader` script below.
6. Choose the appropriate instance size. 
7. Rename the instance.
8. Click 'Create'.

## Launch Loader

```bash
curl -o lightsail-launch.sh https://gitlab.com/abstractable-pub/abstractable-commento/-/raw/master/lightsail-launch.sh
chmod +x ./lightsail-launch.sh
./lightsail-launch.sh
```
